FROM pre_req_project_setup
RUN git clone https://vs083@bitbucket.org/vs083/job_services_private.git
WORKDIR /job_services_private/IndustryPredictor/func1
RUN pip -V
RUN pip install -r requirements.txt
RUN gcc --version
ENV LISTEN_PORT=80
EXPOSE 80
ENTRYPOINT ["python3.6","Test1Api.py"]