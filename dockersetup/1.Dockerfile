FROM centos:latest
RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm
RUN yum -y update
RUN yum install -y python36u python36u-libs python36u-devel python36u-pip
RUN yum -y install wget
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3.6 get-pip.py
RUN yum -y install epel-release
RUN pip --version