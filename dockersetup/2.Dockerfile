FROM centos_python36_pip
RUN yum -y install git 
RUN yum -y group install "Development Tools"
RUN pip install nltk
RUN pip install wheel
