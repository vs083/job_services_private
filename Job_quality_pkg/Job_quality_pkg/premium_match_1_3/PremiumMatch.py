import sys

import os
cwd = os.getcwd()

# ExistFilePath = '/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/ProductionUse/Job_quality_pkg/Job_quality_pkg/premium_match_1_3/PremiumMatch.py'
currentpath = sys.path[0]


# sys.path.insert(0, currentpath)
#print("Current Working Directory " , os.getcwd())
#DirectionAdjust="/premium_match_1_2_kai"

#os.chdir(cwd+DirectionAdjust)
#print("Change to Directory " , os.getcwd())

from .graph_query import get_tokens_id

from .certifications_jobs_education import get_ed_job_cert
from .industry_selector import select_industry_objects

import spacy
import pandas as pd
import pickle
import re
from bs4 import BeautifulSoup
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
stops = set(stopwords.words("english"))
ps = PorterStemmer()


from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from collections import Counter
import numpy as np
import random

from time import time


#from multiprocessing import Pool
#pool = Pool(processes=4)
nlp_object= spacy.load('en', disable=['parser', 'tagger','textcat'])

#curentpath=os.getcwd()
#DirectionAdjust="/premium_match_1_2_kai"


# path to folder contain this packages

with open(currentpath+'/premium_match_1_3/TitleToIndustry_2.pkl', 'rb') as f:
    TitleToIndustry=pickle.load(f)


########################################################################
########################################################################
#
# The goal will be take a list of the jobs description and titles
# attach skill certifications and ect to this DataFrame
#
########################################################################
########################################################################



########################################################################
#Preprocess
########################################################################


print("Starting loading:")


#industry_serial_object = select_industry_objects(ind_list= ["nursing", "education", "healthcare","accounting","finance","office"],nlp_object=nlp)
industry_serial_object = select_industry_objects(ind_list=["nursing", "education", "healthcare","accounting","finance","office"],nlp_object=nlp_object)
_, industry_job_map = get_ed_job_cert()

print(50*"$")


global Ind_JobMatch2
Ind_JobMatch2=dict(TitleToIndustry)

# Create a sample fits 
print("Loading")
random.seed(0)
Allcheck=[]
SampleSize=2000
MatchList1=random.sample(list(Ind_JobMatch2),SampleSize)
Allcheck=Allcheck+MatchList1

MatchList2=random.sample(list(set(list(Ind_JobMatch2)) -set(Allcheck)),SampleSize)
Allcheck=Allcheck+MatchList2

MatchList3=random.sample(list(set(list(Ind_JobMatch2)) -set(Allcheck)),SampleSize)
Allcheck=Allcheck+MatchList3

MatchList4=random.sample(list(set(list(Ind_JobMatch2)) -set(Allcheck)),SampleSize)
Allcheck=Allcheck+MatchList4

MatchList5=random.sample(list(set(list(Ind_JobMatch2)) -set(Allcheck)),SampleSize)
Allcheck=Allcheck+MatchList5
global MatchList_All
MatchList_All=[MatchList1,MatchList2,MatchList3,MatchList4,MatchList5]
        



stops = set(stopwords.words("english"))
def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemma= ps.stem(lemma)
        lemmas.append(lemma)
    return lemmas

def review_to_wordlist(review, remove_stopwords=True ):
    """
        Function to convert a document to a sequence of words,
        optionally removing stop words.  Returns a new doc that got cleaned.
    """

    # 1. Remove HTML
    #print(len(review))
    review_text = BeautifulSoup(review, "html5lib").get_text()
    #print(len(review_text))
    # 2. Remove non-letters
    review_text = re.sub("[^a-zA-Z]"," ", review_text)
    # 3. Convert words to lower case and split them
    words = review_text.lower().split()
    #
    # 4. Optionally remove stop words (false by default)
    if remove_stopwords:
        words = [w for w in words if not w in stops]
    words=lemmatize_verbs(words)
        
    # 5. Return a list of words
    return(" ".join(words))


########################################################################
#Locate Industry
########################################################################
driver_arr=['drive', 'cdl', 'truck',"fleet","van","owner oper","team owner","team owner","deliv","uber","lyft",
            "hazmat","crew","transport","equip oper","mile","road team"]
accounting_arr = ['account',"revenue",'tax',"audit","budget","fund"]
nursing_arr = ['nurse',"nursing","lpn", "lvn"]
healthcare_arr = ['health','care ',"doctor",'dental',"dentist","medical","medicine","anesthesia","hospital","therap","optometrist",
           "surgeon","surgical","practitioner",'therapist','physician',"clinic","biolog","patient",
            'pediatric','anesthesia','phlebotom','psychiat',"radiologic","pharmac","practitioner","urgent",
                 "pathologist","emerg","ortho obgyn","psychologist"]
financ_arr = ['financ','money', 'risk', 'mortgage','payroll','stock','pension','equity',"actuar","credit","econo",
          "data","analyst","invest","insurance","claim","property","casualty","bank","loan"]
education_arr = ['academic','admission', 'school', 'professor','teacher','principal','coach','dean',"educat","financial aid",
                 "econo","instructor","kindergarten","grader","college","library","librarian","tutor","course","facilit","teach",
          "instruct","research","scientist","teach","english educ","educ","mathemat","student","trainer","campus",
                 "lab","chemist"
                ]
office_arr=['associate','administrative', 'administration', 'administrator','office','manager','employee','clerk',"coordinator","financial aid",
          "assistant","bill","coordinator","assistant","receptionist","staff","secretary","director","support",
          "lead","service","agent","planner","advisor","manage","direct","customer","specialist","supervisor",
            "business","strategist","product","editor","java","python","office","engin","tech","softwar","human capit",
           "architect","technician","develop","human resourc","mgr","program","comput","consult","recruit",
           "retail","sale","market","sell","excel"]

other_arr=["club","concierg","bagger","attend","beauti","pool","cashier","meat","seafood","counter","barber",
          "server","artist","design","bartend","cook","packag","cake","chef","mechan","housekeep","contruct","store",
           "warehous","barista","food","repair","hair","beach","store","warehouse","clean","construct","nanni","merchandis",
           "mainten","inspector","host","dishwash","court","lifeguard","loader","bellman","cafe","dealer","washer","pipe",
           "waitress","buyer","bookkeeper" ]
#lemma and stem it
driver_arr=[review_to_wordlist(check) for check in driver_arr]
accounting_arr=[review_to_wordlist(check) for check in accounting_arr]
nursing_arr=[review_to_wordlist(check) for check in nursing_arr]
healthcare_arr=[review_to_wordlist(check) for check in healthcare_arr]
financ_arr=[review_to_wordlist(check) for check in financ_arr]
education_arr=[review_to_wordlist(check) for check in education_arr]
office_arr=[review_to_wordlist(check) for check in office_arr]
other_arr=[review_to_wordlist(check) for check in other_arr]



def Industry_finder_1(Title_i):
    """
    Use exact word match to find industry first 
    """

    Title_i_list=Title_i.split(" ")

    if any(c in Title_i for c in driver_arr): 
        return("driver")

    
    if any(c in Title_i for c in nursing_arr):
        return("nursing")


    if any(c in Title_i_list for c in ["rn","ii"]):
        return("nursing")
        
    if any(c in Title_i for c in healthcare_arr):
        return("healthcare")

    if any(c in Title_i for c in accounting_arr):
        return("accounting")
        
    if any(c in Title_i for c in financ_arr):
        return("finance")
        
    if any(c in Title_i for c in education_arr):
        return("education")

        
    if any(c in Title_i for c in office_arr):
        return("office")

    if any(c in Title_i for c in other_arr):
        return("other")
    
    return(None)


def Industry_finder_2(Title_i,MatchList_All ):
    """
    Find the rest industry by fuzzy match
    Method: 
            1. Random sample 1000 record from Ind_JobMatch2
            2. Find similar job with score cutoff more than 50 
            # not used 4. If any score is over 70, Pick that industry
            5. Find the first industry that show up 3 times
            6. Run over the iteration 5 times 
            7. Go to others

    """
    NeedCount=4
    c=Counter()
    #print(Title_i) MatchList_All=[MatchList1,MatchList2,MatchList3,MatchList4,MatchList5]
    for ML in MatchList_All:
        
        List=process.extractBests(Title_i, [x for x in ML]
                            ,limit=11,scorer=fuzz.ratio,score_cutoff=50)
        global Ind_JobMatch2
        Potential=[Ind_JobMatch2[List_i[0]] for List_i in List]
        if len(Potential)>0:
            c = c+Counter(Potential)
            if c.most_common(1)[0][1]>=NeedCount:
                return(c.most_common(1)[0][0])
                #print(c.most_common(1)[0])
                #print(c)

    if len(c)!=0:
        return(c.most_common(1)[0][0])
    else:
        return("other")
    

def IndustryFinder_summary( Title_i):
    """
    Summary of industry finder 1 2 and inital pickle file
    """
    #global MatchList_All

    global MatchList_All

    if TitleToIndustry.get(Title_i)!=None:
        return(TitleToIndustry.get(Title_i))    
    else:
        Ind_i=Industry_finder_1(Title_i)
        if Ind_i!=None:
            return(Ind_i)
        else:
            Ind_i=Industry_finder_2(Title_i,MatchList_All)
            return(Ind_i)
            
#from concurrent.futures import ProcessPoolExecutor
#def multiprocessing(func, args, workers):
#    with ProcessPoolExecutor(max_workers=workers) as executor:
#        res = executor.map(func, args)
#    return list(res)
from multiprocessing import Pool

def multiprocessing(func, args, workers):
    pool = Pool(processes=workers)
    result = pool.map(func,args)
    pool.close()
    pool.join()
    return result

class Assign_Industry:

    def __init__(self, Title_Lem,CleanDescription2List):
        self.Title_Lem = Title_Lem
        self.CleanDescription2List = CleanDescription2List

    def FindIndustry(self,SavePickleMode=False):
        """
        If its a string input retrun it`s industry

        if its a list of input return a Jobtitle to industry match dictionary

        if Save pickle mode is on, save the dictionary into a pickle file 
        """
        Title_Lem=self.Title_Lem
        #CleanDescription2List=self.CleanDescription2List
        #print("Cleaning Titles: Lemma stem and drop stop")
        if type(Title_Lem)==str:
            Ind_i=Industry_finder_1(Title_Lem)
            if Ind_i!=None:
                return(Ind_i)
            else:
                Ind_i=Industry_finder_2(Title_Lem,MatchList_All)
                return(Ind_i)

        if SavePickleMode:
            #Title_Lem = multiprocessing(review_to_wordlist, Title_Lem, 8)
            TitleToMatch=pd.unique(Title_Lem)
            results =[ IndustryFinder_summary(x) for x in TitleToMatch]
            for i in range(len(TitleToMatch)):
                Ind_JobMatch2[TitleToMatch[i]]=results[i]

            with open('TitleToIndustry.pkl', 'wb') as fp:
                pickle.dump(Ind_JobMatch2, fp, protocol=pickle.HIGHEST_PROTOCOL)
            print("The resulting TitleTOIndustry match is saved at:",os.getcwd()+"/"+'TitleToIndustry.pkl')
            return(Ind_JobMatch2)

        #Title_Lem = multiprocessing(review_to_wordlist, Title_Lem, 8)
        #Title_Lem = pool.map(review_to_wordlist, TitleList)
        TitleToMatch=pd.unique(Title_Lem)
        #print("length of unique Tilte to match ",len(TitleToMatch))
        
        results = [ IndustryFinder_summary(x) for x in TitleToMatch]
        
        #results =pool.map(IndustryFinder_summary, TitleToMatch)
        
        #print("Previous length of matchkey",len(Ind_JobMatch2))
        
        for i in range(len(TitleToMatch)):
            Ind_JobMatch2[TitleToMatch[i]]=results[i]

        return([Ind_JobMatch2[title] for title in Title_Lem] )
        #print("Exporting length of matchkey",len(Ind_JobMatch2))

        #with open('TitleToIndustry.pkl', 'wb') as fp:
        #    pickle.dump(Ind_JobMatch2, fp, protocol=pickle.HIGHEST_PROTOCOL)
        #return(Ind_JobMatch2)
        #print("The resulting TitleTOIndustry match is saved at:",os.getcwd()+"/"+'TitleToIndustry.pkl')


########################################################################
#Get skills 
########################################################################
SubIndustryHash={'Registered Nurses':"nursing",
 'Human Resources'  : 'office',
 'Auditors'  : 'accounting',
 'Occupational and Physical Therapy'  : 'healthcare',
 'Truck Drivers'  : 'driver',
 'Camp Counselors'  : 'office',
 'College and University'  : 'education',
 'Financial Analysts'  : 'finance',
 'Physicians and Surgeons'  : 'healthcare',
 'Pharmacy Technicians'  : 'healthcare',
 'Administrative Assistants'  : 'office',
 'Outside Sales'  : 'office',
 'Warehouse'  : 'office',
 'Nurses'  : 'nursing',
 'Dentistry'  : 'healthcare',
 'Installation and Repair'  : 'office',
 'Technical Support'  : 'office',
 'Sales Management'  : 'office',
 'Network Analysts and Administration'  : 'office',
 'Loan Officers and Counselors'  : 'finance',
 'Electrical and Electronic Engineers'  : 'office',
 'Police Officers'  : 'office',
 'Fast Food'  : 'office',
 'Pest Control'  : 'office',
 'Medical Assistance and Support'  : 'healthcare',
 'Management'  : 'office',
 'Environmental Science'  : 'education',
 'Computer Security'  : 'office',
 'Electrical and Electronics'  : 'office',
 'Medical Technology and Equipment'  : 'healthcare',
 'Store Management'  : 'office',
 'Software Project Managers'  : 'office',
 'Medical Records, Billing and Transcription'  : 'healthcare',
 'Psychologists'  : 'office',
 'Software Development'  : 'healthcare',
 'Preschool'  : 'education',
 'Hosts/Hostesses'  : 'office',
 'Production Supervisors and Managers'  : 'office',
 'Cooks'  : 'office',
 'Accountants'  : 'accounting',
 'Community Service Specialists'  : 'office',
 'Sales'  : 'office',
 'Database Development and Administration'  : 'office',
 'Office Assistants'  : 'office',
 'Customer Service Representative'  : 'office',
 'Industrial Machinery'  : 'office',
 'Government Administration'  : 'office',
 'Interactive Design and Development'  : 'office',
 'Public Relations'  : 'office',
 'Civil Engineers'  : 'office',
 'Material Handlers'  : 'office',
 'Waiters and Servers'  : 'office',
 'Nursing Aides and Attendants'  : 'nursing',
 'Programmers'  : 'office'
}

def GetTokenOfDescription(job_desc,industry,SubIndustryMode=False):
    """
    Take job description
    
    return a dictionary of values
    
    note, the big problem is that not perfectly matching industry
    """

    global TitleToIndustry

    if SubIndustryMode==False: # this is not used anymore
        if industry in ["driver",'other']:
            industry="office"
    else:
        if industry in ["driver",'other']:
            industry="office"
            
    graph_map, combined_matcher = industry_serial_object[industry]["section_graphs"], \
                                  industry_serial_object[industry]["matcher"]
    
    #Test
    #Job_nlp=job_desc
    #Token=get_tokens_id(combined_matcher, Job_nlp ,nlp_object)

    Token=get_tokens_id(combined_matcher, nlp_object(job_desc),nlp_object)
    return(Token)



class Assign_Skills:

    def __init__(self, CleanDescription,industry):
        self.CleanDescription = CleanDescription
        self.industry = industry

    def SkillsFinder(self):
        """
        Clean List
        Drop Stop sentence 
        Drop stopwords lem and stem with Sentence_to_Wordlist
        """
        CleanDescription = self.CleanDescription
        industry = self.industry

        #global TitleToIndustry
        #TitleToIndustry = self.TitleToIndustry

        #print(GetTokenOfDescription(CleanDescription,Title_Lem,industry))
        #print(industry)
        return( GetTokenOfDescription(CleanDescription,industry) )




#print("Current Working Directory " , os.getcwd())
#print("intial wd", IntitialDic)
#os.chdir(IntitialDic)
#print("Change back directory to  " , os.getcwd())
