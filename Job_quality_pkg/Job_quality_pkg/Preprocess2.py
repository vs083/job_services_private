#####################
# The purpose of this package is to preprocess the data into a matrix
# Later on the resulting matrix is used in the train and prediction 
#####################
import pandas as pd
from . import Job_Parsing
from . import FeatureCreation
#import importlib
import os

import numpy as np

import sys
#sys.path.append("../premium_match_1_2_kai")


# ExistFilePath = '/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/ProductionUse/Job_quality_pkg/Job_quality_pkg/premium_match_1_3/PremiumMatch.py'
# AddPath = os.path.dirname(ExistFilePath)
# sys.path.insert(0, AddPath)

import premium_match_1_3.PremiumMatch as PremM
#importlib.reload(PremM)

from functools import partial
#from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from time import time
from itertools import chain
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity
import re

# get group by similarity
from igraph import *
# Pathos can pickle more type of files
# Can be used on multiple processing  
#import pathos.multiprocessing as mp
#from toolz import partition_all
#import spacy
#nlp_object= spacy.load('en', disable=['parser', 'tagger','textcat'])

def UnpackToken_count(Token):
    CSi=Token.get("commonskills")
    Certi=Token.get("certifications")
    hsi=Token.get("hardskills")
    edui=Token.get("education")
    if CSi!=None:
        CSi=len(CSi)
    else:
        CSi=0
        
    if Certi!=None:
        Certi=len(Certi)
    else:
        Certi=0
        
    if hsi!=None:
        hsi=len(hsi)
    else:
        hsi=0
        
    if edui!=None:
        edui=len(edui)
    else:
        edui=0
        
    return([CSi,Certi,hsi,edui])

def CreateX_Matrix(Tuple, DedupMode=False):
    """
    Giving Description and company name
    Return 
        Word_count2
        numbers_count2
        Used_SentenceCount
        Average_Length_Sentence    
    """
    SentenceList=Tuple[0]
    TitleLem=Tuple[1]
    Industry=Tuple[2]

    #print(SentenceList)
    DropSL=Job_Parsing.DropSentence(SentenceList).Checking_words(AddressExtract=True)
    # Sentence after drop stop sentence
    SentenceList2=FeatureCreation.CleanSentence(SentenceList,DropSL).FilterWrongSentence2()

    # number count with sentence dropped stop sentence 
    numbers_count2=FeatureCreation.numbers_count(SentenceList)
    # word count with full list of sentence
    #Word_count3= FeatureCreation.Word_count3(SentenceList)
    # Number of Left sentence
    Used_SentenceCount=len(SentenceList2)
    #Average Length of it 
    Average_Length_Sentence=FeatureCreation.Average_Length_Sentence(SentenceList2)
    # Cleaned descriptions
    #CleanDescription2=" ".join(SentenceList2)

 
    CleanDescription2=FeatureCreation.CleanSentence(SentenceList,DropSL).CleanSentenceList()
    #wordcount of droped sentence
    Word_count2=FeatureCreation.Word_count(CleanDescription2)
    
    #Token finder
    # Note industry is obly different here
    #TitleLem=PremM.review_to_wordlist(Title)
    
    try:

        Token=PremM.Assign_Skills(CleanDescription2, Industry).SkillsFinder()

    except:
        print("Check Industry")
        Industry_finder=PremM.Assign_Industry(TitleLem, CleanDescription2)
        #print(Industry_finder.FindIndustry(SavePickleMode=False))
        Token=PremM.Assign_Skills(CleanDescription2, Industry_finder.FindIndustry(SavePickleMode=False) ).SkillsFinder()
    
    
    List=UnpackToken_count(Token)
    #Word_count2,numbers_count2,Used_SentenceCount, Average_Length_Sentence
    List.append(Word_count2)
    List.append(numbers_count2)
    List.append(Used_SentenceCount)
    List.append(Average_Length_Sentence)

    #print(List)
    
    return(List)



#def multiprocessing(func, args, workers):
#    with ProcessPoolExecutor(max_workers=workers) as executor:
#        res = executor.map(func, args)
#    return list(res)
from multiprocessing import Pool

def multiprocessing(func, args, workers):
    pool = Pool(processes=workers)
    result = pool.map(func,args)
    pool.close()
    pool.join()
    return result

###############################
# For dedup purpuse 
##############################
def CleanSentence(SentenceList):
    """
    Clean sentence keep only words in lower case    
    """
    Returnlist=[]
    for sentence in SentenceList:
        #extract only words
        sentence = re.sub("[^a-zA-Z]"," ", sentence)
        # reformat it and drop duplicate space
        sentence = ' '.join(sentence.split())
        sentence = sentence.lower()

        # 3. Convert words to lower case and split them
        Sentencelength = len(sentence.lower().split())
        if Sentencelength>2:
            Returnlist.append(sentence)
    
    return(Returnlist)

def ExtractSentence(Tuple):
    Description=Tuple[0]
    Company=Tuple[1]
    SentenceList=Job_Parsing.Job_data_clean(Description, Company).SentenceSplit_AdjustHTML()
    return(SentenceList)

def unique_Sentence(lines):
    """
    Get a set of all unique sentences
    """
    return set(chain(*(line for line in lines)))


def SimilarityMatrixCreation(SentenceList):
    """
    Create a Similarity spase matrix giving all the existing sentencelist
    
    """
    
    # Create a set of unique sentences from all jobs
    SentenceList_all=unique_Sentence(SentenceList)
    SentenceList_all = list(SentenceList_all)

    SentenceDictA = dict.fromkeys(SentenceList_all, list(range(len(SentenceList_all))) )
    a =SentenceList_all      #given list
    SentenceDictA={x:i for i,x in enumerate(a)}
    Index=[i for i in range(len(SentenceList))]

    A=[]
    B=[]
    C=[]
    i=0
    for i in Index:
        SentenceList_i=SentenceList[i]
        n=len(SentenceList_i)
        for Sentence in  SentenceList_i:
            A.append(i)
            B.append(SentenceDictA[Sentence])
            C.append(1/n)
    matrix = sparse.coo_matrix((C,(A,B)))
    similarities_sparse = cosine_similarity(matrix,dense_output=False)
    
    return(similarities_sparse)

def CheckStatus(coli,rowi):

    """
    Check if the words count is in reasonable range 
    """
    global Hash_WordCount
    ColWC_i=Hash_WordCount[coli] 
    RowWC_i=Hash_WordCount[rowi]
    #print(ColWC_i,RowWC_i)
    if (ColWC_i<=0.8*RowWC_i) or (ColWC_i>=1.2*RowWC_i):
        return(0)
    else:
        #print("check")
        return(1)



class Preprocess_matrix:

    def __init__(self, Descriptions,Company,Title,TitleToIndustry=None  ):
        self.Descriptions = Descriptions
        self.Company = Company
        self.Title = Title
        self.TitleToIndustry=TitleToIndustry


    def CreateMatrix(self,number_of_processor=8):
        """
        Giving a list of Descriptions Company and title,
        return a matrix 

        """
        #poolObj = mp.Pool(number_of_processor)

        global TitleToIndustry
 
        Descriptions = self.Descriptions
        Company = self.Company
        Title = self.Title
        TitleToIndustry=self.TitleToIndustry

        if type(Descriptions)==str:
            #print("Warning not optimized for one single jobs,")
            Title_Lem=PremM.review_to_wordlist(Title)
            Industry_finder=PremM.Assign_Industry(Title_Lem,Descriptions )
            Industry = Industry_finder.FindIndustry(SavePickleMode=False)
            SentenceList=ExtractSentence(( Descriptions,Company))
            #print((SentenceList,Title_Lem ,Industry))
            Xvec=CreateX_Matrix( (SentenceList,Title_Lem ,Industry) )
            return(Xvec)

        print("Start Finding Industry")
        ST=time()

        # Find industry
        Title_Lem = multiprocessing(PremM.review_to_wordlist, Title, number_of_processor)
        Industry_finder=PremM.Assign_Industry(Title_Lem,Descriptions )
        Industry = Industry_finder.FindIndustry(SavePickleMode=False)
    
        #print(Industry)
        print("Takes ", time()-ST, " second to find Industry")
        print("Start extracting sentences")
        
        # Extract sentences, drop list, cleaned doc
        TupleList=[(a,b) for a,b in  zip(Descriptions,Company)]
        SentenceList=multiprocessing(ExtractSentence,TupleList,8)


        print("Takes ", time()-ST, " second to extract sentences")
        print("Start creating matrix, may take 15ms per job")
        print("commonskills ","certifications ","hardskills ","education ","wordscount2 ","numbercount ","sentencecount "," Ave_sent_length")
        # create matrix 
        TupleList= [(a,b,c) for a,b,c in  zip( SentenceList,Title_Lem,Industry)]
        if len(TupleList)>100:
            print(CreateX_Matrix(TupleList[0]))
        Xmatrix=multiprocessing(CreateX_Matrix,TupleList,number_of_processor)

        return(Xmatrix)

    def CreateMatrix_DedupVersion(self,number_of_processor=8 ):
        """
        Giving a list of Descriptions Company and title,
        return a matrix 

        It is a deduplicate version, which is the same as CreateMatrix except finding duplicate descriptions
        for a set of jobs and only create features for the unique jobs. 
        Then assign the same features for all duplicated jobs.

        May exist a problem when two jobs have exact same description but different titles. Therefore maybe get assign
        into a different industry, which may extract different skills. 
        Also, although I set a limitaion of words difference can not exceed 20% of the length of the documents. 
        There may exists some case thearatically, have words difference that large enough to infulence the resulting scores.

        All the hypothesis should be really rare. I`m assigning 85% of cosine similarities and two documents should have 
        85% of exact same sentences and therefore, they should get assign to a same scores.  

        The time for preprocess the data for 100k job decreaseed from 25mins24s to 13min30s. That leads to a huge increase in 
        speed: from 15.24ms/job to 8.11 ms/job . 

        """
        global Hash_WordCount

        print("Start Finding Industry")
        ST=time()
 
        Descriptions = self.Descriptions
        Company = self.Company
        Title = self.Title

        if any([type(Descriptions)==str,type(Company)==str,type(Title)==str] ):
            raise ValueError("This function does not take str input, try to convert it into list. Please use CreateMatrix functions")


        # Find industry
        Title_Lem = multiprocessing(PremM.review_to_wordlist, Title, number_of_processor)
        Industry_finder=PremM.Assign_Industry(Title_Lem,Descriptions )
        Industry = Industry_finder.FindIndustry(SavePickleMode=False)
    
        
        print("Takes ", time()-ST, " second to find Industry")
        print("Start extracting sentences")
        
        # Extract sentences, drop list, cleaned doc
        TupleList=[(a,b) for a,b in  zip(Descriptions,Company)]
        SentenceList=multiprocessing(ExtractSentence,TupleList,number_of_processor)
        # clean the sentence remove short sentence and keep only letters 
        SentenceList2= [ CleanSentence(s_i) for s_i in SentenceList]

        print("Takes ", time()-ST , " seconds to extract sentences")
        ST=time()

        # Get wordcount 
        Word_count3 = multiprocessing(FeatureCreation.Word_count3, SentenceList, number_of_processor)
        WordCountColumn = Word_count3
        Hash_WordCount={}
        for ind in range(len(Descriptions)):
            Hash_WordCount[ind]=WordCountColumn[ind]

        ## Create Cosine Similarity based on Sentences 
        #select similarity larger than 0.85
        similarities_sparse=SimilarityMatrixCreation(SentenceList2)
        similarities_sparse2 = similarities_sparse.multiply(similarities_sparse >= 0.85)
        Rows,Columns=similarities_sparse2.nonzero()
        
        # Adjust similarity matrix by filtering out large words count difference
        FitFor_WordCount=[CheckStatus(a,b) for a,b in  zip(Columns,Rows)]

        # Create unique groups of similar jobs
        similarities_sparse4 = [(rowi, coli) for rowi, coli, fit_i in zip(Rows,Columns,FitFor_WordCount) if fit_i==1 ]
        g = Graph(similarities_sparse4)
        ClusterStore=g.clusters()

        # Find one representitive in each groups
        Group_Rep= [ i[0]  for i in ClusterStore]
        SentenceList_Rep=[SentenceList[i] for i in Group_Rep]
        Industry_rep = [Industry[i] for i in Group_Rep]

        print("Takes ", time()-ST , " seconds to deduplicate")
        print(len(ClusterStore)," unique jobs out of ", len(SentenceList),"Input jobs")
        print("Start Creating X matrix, estimated time 15ms per job")
        
        TupleList=[(a,b,c) for  a,b,c in zip(SentenceList_Rep,Title_Lem,Industry_rep) ]

        # create matrix 
        print("commonskills  certifications  hardskills  education  wordscount2  numbercount  sentencecount   Ave_sent_length")
        print(CreateX_Matrix(TupleList[0]))
        Matrix_rep=multiprocessing(CreateX_Matrix,TupleList,number_of_processor)
        
        print("Takes ", time()-ST , " seconds to create matrix")
        print("Start Map scores to the duplicated jobs")
        Matrix_Hash={}
        MBlist=ClusterStore.membership
        Matrix_all=[]
        for i in range(len(MBlist)):
            RepGroup_ID = MBlist[i]
            Matrix_Hash[i]=Matrix_rep[RepGroup_ID]
            Matrix_all.append(Matrix_rep[RepGroup_ID])
    
        
        return(Matrix_all)