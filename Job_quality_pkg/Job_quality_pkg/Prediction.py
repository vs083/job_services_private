import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.decomposition import PCA
import pickle

class PCA_Prediction:

    def __init__(self, X_matrix,loaded_model ):
        self.X_matrix = X_matrix
        self.loaded_model = loaded_model


   
    def PredictPCs(self):
        """
        Load PCA model and predict the outcome

        """

        X_matrix = self.X_matrix
        loaded_model = self.loaded_model

        result =loaded_model.transform(X_matrix)

        return(result)

class RandomForest_Prediction:
    
    def __init__(self, X_matrix,loaded_model ):
        self.X_matrix = X_matrix
        self.loaded_model = loaded_model
        
    
    def Predict_Score(self):
        """
        Load Saved RF model and 
        predict scores based on that
        """

        X_matrix_PCs = self.X_matrix
        loaded_model = self.loaded_model
        # try if the input is one or 2 d 
        # if 1d, we transfer into two d so that sklearn can read as input
        try: 
            X_matrix_PCs.shape[1]
        except:
            X_matrix_PCs=np.array([list(X_matrix_PCs)])

        
        Z = loaded_model.predict(X_matrix_PCs)
        Z=[round(zi*100,4) for zi in Z]
        return(Z)