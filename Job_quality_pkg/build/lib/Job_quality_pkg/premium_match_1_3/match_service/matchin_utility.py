from collections import defaultdict
from nltk.stem import PorterStemmer

from match_service.graph_query import  get_tokens_id, get_subgraph_data, get_sections_data
from fuzzywuzzy import fuzz
from configuration.config import Config
from pymongo import MongoClient

config_obj = Config()
mongo_conn = config_obj.mongo_connection
client = MongoClient(mongo_conn)

def match_doc_sections(section_name, job_section, graph_map):

    collect_intervals = defaultdict()


    for item in job_section:

        try:

            intervals = get_subgraph_data(item, graph_map[section_name])

            intervals = [i for i in intervals if i]

            collect_intervals[item] = intervals

        except BaseException as e:
            #print("matching util line 31",e)
            collect_intervals[item] = []


    return collect_intervals

def sections(section_name, job_section, graph_map):

    collect_intervals = defaultdict()


    for item in job_section:

        try:

            intervals = get_subgraph_data(item, graph_map[section_name])

            intervals = [i for i in intervals if i]

            collect_intervals[item] = intervals

        except BaseException as e:
            #print("matching util line 31",e)
            collect_intervals[item] = []


    return collect_intervals


def match_and_score(interval_list):

    score_collector = defaultdict(list)

    for intervals in interval_list:
        if interval_list[intervals]:

            for keys in interval_list[intervals]:

                for sub_list in keys:

                    del sub_list[sub_list.index(intervals)]

                    score_collector[intervals].append(sub_list)

        else:
            score_collector[intervals] = []

    return score_collector

def fuzzy_match(key, candidate_list):
    fuzz_set = []

    for items in candidate_list:
        ratio = fuzz.token_set_ratio(key, items)

        if ratio > 40:
            fuzz_set.append(items)

    return fuzz_set


def score_relevance(section_relevance_dict, resume_dict,phrase_matcher=None, nlp_object=None, is_certifications = False):

    if is_certifications == True:
        cert_list = " ".join(resume_dict)


        resume_dict = get_tokens_id(phrase_matcher, nlp_object(cert_list), nlp_object)['certifications']


    section_socred_dict = {}

    exact_match = []
    partial_matches = {}


    for keys in section_relevance_dict:


        if keys in resume_dict:

            exact_match.append(keys)
            partial_matche = fuzzy_match(keys, resume_dict)

            partial_matches[keys] = partial_matche


        else:

            partial_matche = fuzzy_match(keys, resume_dict)

            partial_matches[keys] = partial_matche

            candidates = section_relevance_dict[keys]

            for i, j in candidates:

                if i in resume_dict:

                    if j > 0.5:
                        section_socred_dict[keys] = section_socred_dict.get(keys,'') + i
                        #section_socred_dict[keys].append(i) #add item and or score

    sorted_section = sorted(section_socred_dict.items(), key=lambda x: x[1])

    return dict(sorted_section), exact_match, partial_matches



def extract_sections(res_id, job_desc, phrase_matcher, nlp_object, graph_map, job_title = None):

    res_data = get_sections_data(res_id, client)

    final_dict = {}


    phrases = get_tokens_id(phrase_matcher, job_desc, nlp_object)

    job_section_dict = {"JobTitles": [job_title],
                        "JobSkills": phrases['hardskills'],
                        "CommonSkills": phrases['commonskills'],
                        "EducationProfile": phrases['education'],
                        "Certifications": phrases['certifications']}

    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(job_section_dict)
    #print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    for keys in job_section_dict:
        flag = False
        job_section = job_section_dict[keys]

        res_section = res_data[keys]

        intervals = match_doc_sections(keys, job_section, graph_map)


        relevant_skills = match_and_score(intervals)


        if keys == "Certifications" :
            flag = True

        # if keys == "JobTitles":
        #     print(relevant_skills, res_section)
        sectional_scores, exact_match, partial_match = score_relevance(relevant_skills, res_section,phrase_matcher,
                                                                       nlp_object, is_certifications=flag)

        final_dict[keys] = {"secondary":sectional_scores, "primary":exact_match,"partial": partial_match}

    job = strength_weaknes(final_dict, job_section_dict, "JobTitles")

    hard = strength_weaknes(final_dict, job_section_dict, "JobSkills")

    common = strength_weaknes(final_dict, job_section_dict, "CommonSkills")

    certifications = strength_weaknes(final_dict, job_section_dict, 'Certifications')

    education = score_education_profile(res_data, job_desc, phrase_matcher, nlp_object)

    return hard, common, education, certifications, job

def strength_weaknes(sectional_final_dict, raw_job_skills, section_name):

    scored_section = sectional_final_dict

    job_skills = scored_section[section_name]

    primary_job_skills = job_skills['primary']
    secondary_skill = job_skills['secondary']
    partial_skill = job_skills['partial']


    #unscored_jobs_skills = raw_job_skills[section_name]
    if len(primary_job_skills) > 0 :
        return {"strengths+exact match": primary_job_skills, "related strengths": secondary_skill,
                "partial": partial_skill}
    else:
        #print("no exact match was found for ", section_name )
        return {"strengths+exact match": primary_job_skills, "related strengths": secondary_skill,
                "partial":partial_skill}




def score_job_titles(res_dict, job_job_title, job_subgraph):

    exact_match = []

    for jobs in res_dict["JobTitles"]:

        for jbs in jobs:

            if job_job_title == jbs:

                exact_match.append(jbs)

        intervals = match_doc_sections("JobTitles", jobs, job_subgraph)

    return  {"exact_match": exact_match, "related jobs" : match_and_score(intervals)}


def score_education_profile(res_dict, job_desc, phrase_matcher, nlp_object):
    stemmer = PorterStemmer()
    master = ["master",",masters", "m.a.", "m.a","m.s.","m.s","m.b.a", "mba","ma","ms","msn",]
    bachelor = ["bachelor","bachelors", "b.a.", "b.a","b.s.","b.s","ba","bs", "bsn","graduate", "graduat"]
    associate = ["associate", "associates","associ"]
    highschool = ["high school diploma", "high school","ged","high"]


    rank = ''
    job_education_raw = list(get_tokens_id(phrase_matcher, job_desc, nlp_object)['education'])
    res_education_raw = res_dict["EducationProfile"]
    job_education = set([stemmer.stem(ed.split()[0]).replace("'", "") for ed in job_education_raw])
    res_education = [stemmer.stem(ed.split()[0]).replace("'", "") for ed in res_education_raw]
    # print(res_education, job_education)


    if any([i in master for i in res_education])==True and any([i in master for i in job_education])==True:
        rank = ("masters with master" ,"same")

    elif any([i in master for i in res_education])==True and any([i in master for i in job_education]) == False:
        rank = "masters over job","over qualified"

    elif any([i in master for i in res_education])==False and any([i in master for i in job_education]) == True:
        rank = ("masters under job ", "under qualified")

    elif any([i in bachelor for i in res_education])==True and (any([i in master for i in job_education])==True):
        rank = ("masters under job", "under qualified")

    elif any([i in bachelor for i in res_education])==True and (any([i in bachelor for i in job_education]) == True \
            and any([i in master for i in job_education]) == False):
        rank = ("bachelors with bachelors", "same")

    elif any([i in bachelor for i in res_education])== True and (any([i in master for i in job_education]) == False \
            and any([i in bachelor for i in job_education])==False):
        rank = ("bachelor over job", "over qualified")


    elif any([i in associate for i in res_education])==True and  (any([i in master for i in job_education])==True \
            or any([i in bachelor for i in job_education]) == True):

        rank = ("associate under job", "under qualififed")

    elif any([i in associate for i in res_education])==True and (any([i in master for i in job_education])==True or
            any([i in bachelor for i in job_education]) == True):

        rank = ("associate under associate", "under qualified")


    elif any([i in associate for i in res_education])==True and (any([i in master for i in job_education]) == False \
            and any([i in bachelor for i in job_education]) == False \
            and any([i in associate for i in job_education]) == True):

        rank = ("associate over job", "over qualified")


    elif any([i in highschool for i in res_education])==True and (any([i in master for i in job_education])==True or
                                                            any([i in bachelor for i in job_education]) == True or
                                                            any([i in associate for i in job_education])==True):
        rank = ("high school under job", "under qualififed")


    elif any([i in highschool for i in res_education])==True and any([i in highschool for i in job_education])==True \
            and any([i in master for i in job_education])==False and any([i in bachelor for i in job_education]) == False\
            and any([i in associate for i in job_education])==False:
        rank = ("high school with high school", "same")

    elif res_education=='' or res_education==[]:
        rank = "No education found"

    return rank


def convert_to_score(match_output):
    partial_count = 0
    exact_match = match_output['strengths+exact match']
    partial_match = match_output["partial"]
    related_match = match_output['related strengths']

    total_num_items = len(partial_match)

    if total_num_items > 0:

        exact_score = len(exact_match)/total_num_items

        for item in partial_match:
            lists = partial_match[item]

            if len(lists) > 0:
                partial_count += 1

        partial_score = partial_count/total_num_items
        related_score = len(related_match)/ total_num_items

    # add +1 smoothing to avoid division by zero error
    elif total_num_items == 0:
        exact_score = len(exact_match)/(total_num_items+0.1)

        for item in partial_match:
            lists = partial_match[item]

            if len(lists) > 0:
                partial_count += 1

        partial_score = partial_count/(total_num_items+0.1)
        related_score = len(related_match)/ (total_num_items+0.1)

    if exact_score == 1:

        combined_score = exact_score + 0.5 * partial_score + 0.5 * related_score

        return max(combined_score, 1)

    else:

        combined_score = exact_score + partial_score + related_score

        return min(1,combined_score)
