########
# Use saved model to predict scores
# given inputs of Descriptions Company and Title
########

import pickle
from . import Preprocess2, Prediction


# Load saved pca model
filename="SavedModel_PCA_RF/PCA_Model_init.sav"
loaded_PCA = pickle.load(open(filename, 'rb')) 


# Load saved random forest model
filename="SavedModel_PCA_RF/RandomForest_init.sav"
loaded_RF = pickle.load(open(filename, 'rb')) 


class Job_quality:

    def __init__(self, Descriptions,Company,Title ):
        self.Descriptions = Descriptions
        self.Company = Company
        self.Title = Title
    def ScorePrediction(self,number_of_processor=8,SingleJobMode=False ):

        """
        Giving a list of Descriptions Company and title,
        return a score that represent the quality of the jobs 
        """

        Descriptions = self.Descriptions
        Company = self.Company
        Title = self.Title

        if SingleJobMode:
            PreProcessor=Preprocess2.Preprocess_matrix(Descriptions,Company,Title)
            Matrix_X=PreProcessor.CreateMatrix(number_of_processor=number_of_processor)
            PCs=Prediction.PCA_Prediction([Matrix_X],loaded_PCA).PredictPCs()
            CheckPredictions=Prediction.RandomForest_Prediction(PCs[0],loaded_RF).Predict_Score()
            return(CheckPredictions[0])

        
        PreProcessor=Preprocess2.Preprocess_matrix(Descriptions,Company,Title)
        Matrix_X=PreProcessor.CreateMatrix_DedupVersion(number_of_processor=number_of_processor)
        PCs=Prediction.PCA_Prediction(Matrix_X,loaded_PCA).PredictPCs()
        CheckPredictions=Prediction.RandomForest_Prediction(PCs,loaded_RF).Predict_Score()
        return(CheckPredictions)
