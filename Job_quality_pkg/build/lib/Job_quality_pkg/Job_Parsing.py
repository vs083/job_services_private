import pandas as pd
import numpy as np

import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup

ps = PorterStemmer()
stops = set(stopwords.words("english"))


import html2text
from commonregex import CommonRegex
import pyap
import re

from IPython.core.display import display, HTML
from bs4 import BeautifulSoup


def SentenceSplit_LongSentenceMode(TryThisOut,CompanyName):
    """
    Logic:
    1. find out words that start with capital letters
    2. treat words like NASDAC as one single word
    3. Interjections Determiner will collapse with next sentence
    4. "'s" will collapse with next sentence
    5. Sentence shorter than 6 words will collapse with next short sentence if it`s exist.
    6. Check if company name, collapse
    """
    if type(CompanyName) != float:
        CompanyNameList= CompanyName.split(" ")
    else: 
        CompanyNameList=[]
    
    s2=re.findall('[A-Z]*[^A-Z]*', TryThisOut)
    s3=[ [ x for x in s2i.split(" ") if x !=""] for s2i in s2]
    s4=[]
    BeginWords=""
    i=0
    CheckMark=False
    CheckMark2=0
    CheckMark3=0
    CheckMark4=0

    for s3i in s3:
        i=i+1
            
        ## type of the last words check not "DT" or "IN"
        try:
            if nltk.pos_tag([s3i[-1]])[0][1] in ["IN","DT"] or s3i[-1][-2:]=="'s":
                #print(s3i,s3[i],CheckMark2)
                if BeginWords!="" and len(BeginWords.split(" "))>=6 and i!=CheckMark2+1:
                    s4.append(BeginWords)
                    BeginWords=""
                    
                BeginWords=BeginWords+" "+" ".join(s3i)
                CheckMark=True
                CheckMark2=int(i)
                if nltk.pos_tag([s3i[-1]])[0][1] == "DT":
                    CheckMark4=int(i)
                continue
                
        except Exception as e: 
            #print(e)
            pass
        
        ## Adjust company name
        try:
            if s3i[-1] in CompanyNameList and s3[i][0] in CompanyNameList:
                #print(s3i,s3[i])
                
                if BeginWords!="" and i!=CheckMark3+1:
                    #print(BeginWords,i,CheckMark3)
                    s4.append(BeginWords)
                    BeginWords=""
                    
                BeginWords=BeginWords+" "+" ".join(s3i)
                CheckMark=True
                CheckMark3=int(i)
                continue  
                    
        except Exception as e: 
            #print(e)
            pass
        
        if len(s3i)<6 or CheckMark or (i == CheckMark4+2 and len(s3[i-2])==1):
            BeginWords=BeginWords+" "+" ".join(s3i)
            #print(BeginWords)
        else:
            if BeginWords!="":
                s4.append(BeginWords)
                BeginWords=""
            s4.append(" ".join(s3i))
            
            
        if (i==len(s3)) & (len(s3i)<6):
            s4.append(BeginWords)
            BeginWords=""
            
        CheckMark=False
        
        
    return(s4)


caps = "([A-Z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov|edu)"
websites2 = "(www|WWW)[.]"
websites3 = "(https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+)[.]"
websites4 = "[.](com|net|org|io|gov|edu)"

def split_into_sentences(text,CompanyName,HTML=False,LargeSentenceAdjustMode=True):
    if HTML==False:
        text = text.replace("\n","<stop>")
    text = " " + text + "   "
    text = text.replace("\n","  ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    text = re.sub(websites2,"\\1<prd>",text)
    text = re.sub(websites3,"\\1<prd>",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + caps + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + caps + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("   ","<stop>")
    text = text.replace(";","<stop>")
    text = text.replace("<prd>",".")    
    text = text.replace(" * ","<stop>* ")
    text = text.replace("###","<stop>")
    
    # Specially for long sentence split
    text = text.replace("To apply","<stop>To apply")

    #print(text)
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    
    
    ### Adjust Large Chunck Of Words
    ### Split only when Sentences length is too large 
    ## 16776/ 232/ 60041/4207/52387
    if LargeSentenceAdjustMode:
        sent2=[]
        for sentence in sentences:
            if len(sentence.split(" "))<=40:
                sent2.append(sentence)
            else:
                sent2=sent2+SentenceSplit_LongSentenceMode(sentence,CompanyName)

        sentences=sent2

    sentences = [s.strip() for s in sentences]
    return sentences


def Sentence_to_Wordlist(review, remove_stopwords=True ):
        """
            Function to convert a sentence to a sequence of words,
            optionally removing stop words.  Returns a new doc that got cleaned.
            
        """

        # 1. Remove HTML, which is already done in sentence split 
        #print(len(review))
        #review_text = BeautifulSoup(review, "html5lib").get_text()
        #print(len(review_text))
        # 2. Remove non-letters
        review_text = re.sub("[^a-zA-Z]"," ", review)
        # 3. Convert words to lower case and split them
        words = review_text.lower().split()
        # 4. Optionally remove stop words (false by default)
        if remove_stopwords:
            words = [w for w in words if not w in stops]
        words=lemmatize_Stem_words(words)
        # 5. Return a list of words
        return(" ".join(words))

def lemmatize_Stem_words(words):
        """
        Lemmatize verbs in list of tokenized words
        Stem it after that 
        """
        lemmatizer = WordNetLemmatizer()
        lemmas = []
        for word in words:
            lemma = lemmatizer.lemmatize(word, pos='v')
            lemma= ps.stem(lemma)
            lemmas.append(lemma)
        return lemmas

def FilterWrongSentence(Sentencelist):
    """
    Filter out sentence list are empty and have only one letter
    """
    Sentencelist=list(filter(None, Sentencelist))
    Sentencelist=[s for s in Sentencelist if len(s)>1]
    return(Sentencelist)

class Job_data_clean:

    def __init__(self, JobDescription, CompanyName):
        self.JobDescription = JobDescription
        self.CompanyName = CompanyName


    def PrintFile(self):
            #Give a file print it 
        print(39*"@@@")
        HtmlMessage=self.JobDescription
        if bool(BeautifulSoup(HtmlMessage, "html.parser").find()):
            HtmlMessage=HtmlMessage.replace("$", "Dollar")
            display(HTML(HtmlMessage))
        else:
            print(HtmlMessage)
            
   
    def SentenceSplit_AdjustHTML(self,StemLem=False):
        
        """
        #Give a file and split into sentences 
        ## poential problem if a sentences is ended by spaces, Model can not tell

        #if "driver" not in  [x.lower() for x in MasterTable.Title[i].split()] and "cdl-a" 
        # not in  [x.lower() for x in MasterTable.Title[i].split()]:
        
        
        
        Speed: 2min 45s for 10 k jobs 
        """    

        HtmlMessage=self.JobDescription
        
        CompanyName=self.CompanyName

        if bool(BeautifulSoup(HtmlMessage, "html.parser").find()):
            
            h = html2text.HTML2Text()
            h.ignore_images = True
            h.ignore_links = True
            HtmlMessage2 = h.handle(HtmlMessage)
            HtmlMessage2
            if len(HtmlMessage2)<2:
                HtmlMessage2=BeautifulSoup(HtmlMessage, "html5lib").get_text()
            
            SentenceSplit=split_into_sentences(HtmlMessage2,CompanyName,True)
        else:
            SentenceSplit=split_into_sentences(HtmlMessage,CompanyName)
        
        if StemLem:
            # Stem and lemtize the sentence and clean the sentence
            SentenceSplit=[Sentence_to_Wordlist(s) for s in SentenceSplit]
        
        return(FilterWrongSentence(SentenceSplit))


####################################################################################################################
####################################################################################################################
####################################################################################################################
###
### Drop sentences
###
####################################################################################################################
####################################################################################################################
####################################################################################################################
CheckList3=["religion"," sex "," gender","lgbt","equal opportun","fair labor","discrimin","marital status",
            "equal employ" ,"nondiscrimin","diverse workforce","associated topics:","veteran","eeo statement"]
# contact info
CheckList4=CheckList3+["please contact","please call","job posting id","reference id","apply now",
                         "if you need assistant","please visit","view our","visit our","apply online","learned about a position",
                         "learned about this position" , "thank you for your","interested in this opportunit",
                        "interested in the opportunit","or more more detail","we look forward to","send your resume",
                      "jobopenings=","give us a call"]
#jobopenings is for one link type

CheckList5=CheckList4+["date posted:", "contact:","employment type:","application deadline:",
                       "start date:","zip code:","requisition id:","job id:","req id:","reqid:"]
    
def PrintSentenceInfo(Index,Length,Sentence):
    print(Sentence)
    print("Sentence Location: ",Index,"/ Number of Sentence",Length,"/")
    try:
        print("_____Nearby Sentence______")
        #print(SentenceList[Index-1])
        
    except:
        try:
            print(Sentence)
            #print(SentenceList[Index+1])
        except:
            pass
        
def detect_numbers(text):
    phone_regex = re.compile(r"(\+420)?\s*?(\d{3})\s*?(\d{3})\s*?(\d{3})")
    groups = phone_regex.findall(text)
    
    return(["".join(g) for g in groups])



class DropSentence:

    def __init__(self, Sentences):
        self.Sentences = Sentences
  
    def Checking_words(self,CheckList=CheckList5,CheckSentence=False,AddressExtract=True):
        """
        Give a Sentence List from a job description.
        
        Drop 1. Equal employment relative sentence
            2. Associate Topics: related sentence which is very commend and redundent information,
            3. Maybe in the furture: Drop contact information/ contact info: email, phone, website/ Apply now and etz
            
        Rule: Check the CheckList if any words is included in each sentence: 
                if only veteran is contained in the sentence, filter out 
                    1. Long sentence which could refer to different information.(sentence lengther >20)
                    2. Sentence that contain in AntiVetList which usually show up in nursing jobs 
                    3. if words in VetList show up:
                            it will be a equal employment sentence
        Speed: 48.9 s for 10k jobsDescriptions  
            : + 2min 45 s for sentence split from documents 
        """

        SentenceList=self.Sentences

        # Lematize and stem the check list
        #CheckList=lemmatize_Stem_words(CheckList)
        
        # List of words that free veteran
        AntiVetList=["care ","caring","nurse","treatment"] 
        #+ [x for x in CheckList2 if x!= "veteran"]
        ## List of words that trap veteran
        VetList=["hire ","hiring","employ","honor","eoe ","disable"] 
        
        
        AntiContactList=["requirment","year","month","qualification","requirement","skill","equipment","$","dollar","hour"]
        
        ContactList=["please contact","please call",'call today',"job posting id","reference id","apply now",
                            "if you need assistant","please visit","view our","visit our","apply online","learned about a position",
                            "learned about this position" , "thank you for your","interested in this opportunit",
                            "interested in the opportunit","or more more detail","we look forward to","send your resume"]
        ContactList2=["date posted:", "contact:","employment type:","application deadline:",
                        "start date:","zip code:","requisition id:","job id:","req id:","reqid:"]

        
        ExtractList=[]
        #Index=[]
        
        if len(SentenceList)==1:
            
            if CheckSentence:
                print("Exist a unable to split sentence")
                
                print(SentenceList)
            return(None)
        Doc=' '.join(SentenceList)
        DocLength=len(Doc.split(" "))
        
        
        for sentence in SentenceList:
            
            # If a sentence is larger than 60% of that job length, we let it go 
            SentLength=len(sentence.split(" "))
            if SentLength > DocLength*0.6:
                continue
                
            #################################################################################   
            # check contact part 2 email phone address check
            if len(sentence)>0:
                
                parsed_text = CommonRegex(sentence) 
                #+len(parsed_text.dates)
                
                
                # RE check for contacts 
                StreetCheck1= parsed_text.street_addresses
                PhoneCheck1= parsed_text.phones
                EmailCheck1=parsed_text.emails
                LinksCheck1=parsed_text.links
                DateCheck1=parsed_text.dates

                
                # Many of the cases, Street check is misleading 
                # We will further check it with a more restricted packages
                # StreetCheck2=pyap.parse(sentence, country='US')
                
                if AddressExtract:
                    RECheckList=StreetCheck1
                else:
                    RECheckList= []
                
                RECheckList = RECheckList + PhoneCheck1+ EmailCheck1+LinksCheck1+DateCheck1

                
                if ( len(RECheckList)>0) & all([sentence.lower().find(avl)==-1 for avl in  AntiContactList]):
                    
                    #print(RECheckList)
                    #print(parsed_text.phones, parsed_text.emails,parsed_text.links)
                    
                    # double check the address extraction
                    if AddressExtract:
                        if(len(StreetCheck1)!=0 ):
                            #print(StreetCheck1)
                            #print(sentence)
                            StreetCheck2=pyap.parse(sentence, country='US')
                            #print(StreetCheck2)
                            ZipFinder=re.findall('(\d{5})([- ])?(\d{4})?', sentence)
                            if len(StreetCheck2)>0 and len(ZipFinder)>0 :
                            #if FullAddressCheck(sentence) :    
                                #print("**"*10)
                                #print(sentence)
                                ExtractList.append(sentence)
                                continue

                    # Double check Phone Number only select the one with more than 10 digits 
                    CheckPhone=False
                    if len(PhoneCheck1)>0:
                        for PhoneCi in PhoneCheck1:
                            Digit="".join(re.findall("[0-9_]+", PhoneCi))
                            if len(Digit)>=10:
                                #print(PhoneCheck1)
                                CheckPhone=True
                    if CheckPhone==True:
                        ExtractList.append(sentence)
                        continue
                        
                    if len(EmailCheck1+LinksCheck1)>0:
                        ExtractList.append(sentence)
                        continue
                    if len(DateCheck1)>0:
                        ExtractList.append(sentence)
                        continue
                    
            #################################################################################       
            # Check by words 
            for Check in CheckList:

                if (sentence.lower().find(Check)!=-1 ):
                    
                    
                    #print(Check)
                    
                    #Index=SentenceList.index(sentence)
                    
                    # veteran check
                    if Check== "veteran":
                        if Doc.lower().count('veteran')>5:
                            continue
                            

                        if all([sentence.lower().find(avl)==-1 for avl in  VetList ]) & (
                            any([sentence.lower().find(avl)!=-1 for avl in  AntiVetList ]) | (len(sentence.split(" "))>20)):
                            continue
                        else:
                            ExtractList.append( sentence)  
                            break
                    else:   


                        # check contact word check
                        if (any([sentence.lower().find(avl)!=-1 for avl in  AntiContactList ])
                            )& (Check in ContactList):
                            #print(sentence)
                            continue
                        
                        # check contact part 2 word check
                        if Check in ContactList2:
                            #print(sentence)
                            if len(re.findall("[a-zA-Z_]+", sentence))>=10:
                                #print("!!!!!!!"*10)
                                #print(sentence)
                                
                                continue  
                                
                        #if Check in ["zip code:","id:","eeo statement"]:
                            #print(Check,sentence)
                        
                        ExtractList.append(sentence)
                        break        
                
        
        if len(ExtractList)==0:
            return(None)
        return(ExtractList)
        