# Name #

## Job Quality Package ##

This application is used as a method of identifying the low quality jobs and assigning a score for a given jobs. This service goes beyond words counts method to provide more relevant measurements of the quality of jobs.

This methods filters out irrelevent descriptions such as Equal Employment Act, Contact information, and Auto Gerenated summary(Associate Topics:). Taking the following features into considerations, pass them through PCA to reduce dimension, and predict the quality of jobs using Random Forest. 

The current training data sets are 300 self rated jobs with binary response,like or not. The benefits of this methods is that it can accuratly identify the bad quality jobs. The limitation of this method is that the majority of the good jobs have a very close rating. This can be improved by using a different set of training data with larger ranges of response rather than Binary response. 

More features of this application are under construction.

# Prerequisites #

- Full list of packages in the requirements.txt
- Python 3.6 or 3.4.3
- Pip (latest verion)               


# Structure #

- Job_Parsing.py  # Sentence Split and drop
- FeatureCreation.py # Create features based on SentenceList and Drop list
- premium_match_1_2_kai/PremiumMatch.py # Finding skills certification educations based on Job description and industry
                                        # Current Version is still using spacy, need to adjust next step 
- Preprocess2   # Preprocess the data using the above 3 
- Training.py # Training model based on features 
- SavedModel_PCA_RF #File that saved previously trained model 
- Prediction.py # Predict Scores based on trained model

## Data ##

The current training data sets are 300 self rated jobs with binary response,like or not. PCA is trained from 100K jobs.

# Other Detailed information #

Please read the README.md file in job_services folder

# Who do I talk to? #

* Repo owner or admin