import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("Requirements.txt", "r") as fh:
    requirements = fh.read()

setuptools.setup(
    name="Job_quality_pkg",
    version="0.0.6.1",
    author="Kai Jin",
    author_email="kaij5088@gmail.com",
    description="A package that is desgeign to rate a job post given job description, title, company",
    long_description=long_description,
    long_description_content_type="text/markdown",
    #url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    install_requires= requirements.split('\n'),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)