from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='IndustryPredictor',
      version='0.0.2',
      description='Predict industry out of 51 industries given Job description, Title, and Company',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='Kai Jin',
      author_email='kai.jin@Bold.com',
      license='MIT',
      packages=['IndustryPredictor'],
      zip_safe=False)