import logging
import flask
import json
from os import path
import sys
import os
from flask import jsonify 
from flask import request
import pandas as pd
import nltk
import socket
sys.path.append('..')
#sys.path.append(path.abspath('D:\IndustryPredictorVirtualEnv\IndustryPredictor'))

nltk.download("stopwords")
nltk.download("wordnet")

from PredictionApp import IndustryPrediction
from blockBlobService import blockBlobHelper

blobHelper = blockBlobHelper()
blobHelper.downloadBlobFromContainer('industrymodel')

basePath=os.path.abspath("..")
model_path = basePath+"/func1/ml-model-20190215/ml-model/"

obj = IndustryPrediction()
obj.load_model(model_path)

app = flask.Flask(__name__)
app.config["DEBUG"] = True



@app.route('/', methods=['GET'])
def home():    	
	#description=desc
	#company = com
	#title = ti
	#n=5 # number of industries that you need
	#a = obj.predict_industry(description, company, title, n)
	return jsonify(socket.gethostname())


@app.route('/predict',methods = ['POST'])
def predict():
	#print (request.json)
	#print(f"Python BasePath=============> {basePath}")
	#print(f"Model Path     =============> {model_path}")
	#print('Python HTTP trigger function processed a request.')
	reqdata = request.get_json(force=True)
	description = reqdata['description']
	company = reqdata['company']
	title = reqdata['title']
	n=5
	response = obj.predict_industry(description, company, title, n)	
	#return jsonify(reqdata)
	return jsonify(response)

#app.run()
app.run(host='0.0.0.0',port=80)