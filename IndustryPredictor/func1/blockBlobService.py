import os
from azure.storage.blob import BlockBlobService

class blockBlobHelper:

    def downloadBlobFromContainer(self,container_name):
        try:
            # Create the BlockBlockService that is used to call the Blob service for the storage account
            block_blob_service = BlockBlobService(account_name='testresumesearchstorage', account_key='eEy9BgcTK5nDsUz6b05HPX+1+kLACgYTgiboDi+1u6I5m57ji5wjsh4agMmxDmpFPYveLcdUvtJrxGgdYZNpNA==')

            # Create a container called 'quickstartblobs'.
            #container_name ='industrymodel'
            #block_blob_service.create_container(container_name)

            # Set the permission so the blobs are public.
            #block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

            # Create a file in Documents to test the upload and download.
            basePath=os.path.abspath("..")+"/func1/"
            print(basePath)
            # List the blobs in the container
            print("\nList blobs in the container")
            generator = block_blob_service.list_blobs(container_name)
            for blob in generator:
                print("\t Blob name: " + blob.name)
                full_path_to_file2 = os.path.join(basePath, blob.name)
                print("\nDownloading blob to " + full_path_to_file2)
                if not os.path.exists(os.path.dirname(full_path_to_file2)):                
                    os.makedirs(os.path.dirname(full_path_to_file2))

                block_blob_service.get_blob_to_path(container_name, blob.name, full_path_to_file2)

        except Exception as e:
            print(e)