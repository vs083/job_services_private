import logging
import azure.functions as func
import json
import os
import sys
import nltk

import sys
sys.path.append('..')

#nltk.download("stopwords")
#nltk.download("wordnet")

from PredictionApp import IndustryPrediction
from blockBlobService import blockBlobHelper

#blobHelper = blockBlobHelper()
#blobHelper.downloadBlobFromContainer('industrymodel')

obj = IndustryPrediction()
#print(type(obj))

basePath=os.path.abspath("..")
model_path = basePath+"/ml-model-20190215/ml-model/"
obj.load_model(model_path)

def main(req: func.HttpRequest) -> func.HttpResponse:	
	logging.info(f"Python BasePath=============> {basePath}")
	logging.info(f"Model Path=============> {model_path}")
	logging.info(f"Python Obj =============> {type(obj)}")
	logging.info('Python HTTP trigger function processed a request.')
	req_body = req.get_json()
	description=req_body.get('description')
	company =req_body.get('company')
	title =req_body.get('title')
	n=5
	response = obj.predict_industry(description, company, title, n)
	return func.HttpResponse(f"{json.dumps(response)}")
#	return func.HttpResponse(f"loaded !")